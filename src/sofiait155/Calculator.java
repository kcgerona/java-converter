package sofiait155;


import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LENOVO
 */
public class Calculator {
    
    public Map binary(String input){
    	
        int convertnum, result;
        Map<String, String> stringresult = new HashMap<>();
        if(!input.matches("[01]+") ||  input.isEmpty() ){
            stringresult.put("error", "Invalid binary input.");
        }else{
            convertnum = Integer.parseInt(input);
            stringresult.put("decimal", String.valueOf(this.convertToDec(convertnum, 2)));
            stringresult.put("octal", this.convertFromBin(convertnum, 8));
            stringresult.put("hexadecimal",this.convertFromBin(convertnum, 16));
        }
        
        return stringresult;
    }
    public int convertToDec(int somenum, int base){ 
    	/* THIS FUNCTION CONVERTS BINARY AND OCTAL NUMBERS INTO DECIMAL
    	 * (1) Divide number by place
    	 * (2) For each number, multiply it by increasing exponential value of base (2, 8 or 16)
    	 * (3) Add products for the result  
    	 * ex: 724 [base 8] => (4 x 8^0 = 4)+(2 x 8^1 = 16)+(7 x 8^2 = 448) = 468 [base 10] 
    	 */
    	int result = 0; //end value
        int i = 0;
        int y, octalbinhex, digit;
        while (somenum > 0) { //while somenum hasn't been fully divided into parts
          y = 0;
          octalbinhex = 1;
          while (y < i) {
        	octalbinhex = octalbinhex * base; // Octal/Bin is base to the power of y
            y++;
          }
          digit = somenum % 10; // (1): Digit is the yth digit or the last number of somenum
          somenum = somenum / 10; // Remove digit from somenum by dividing somenum by 10 (last digit is dropped)
          result = result + (digit * octalbinhex); //(2) & (3): Result is result plus (Digit * octalbinhex)
          i++;
        }
        return result;
    }
    public String convertFromBin(int somenum, int base){
    	/* THIS FUNCTION CONVERTS BINARY INTO OCTAL AND HEXADECIMAL
    	 * (1) Divide binary by 3 (octal) or 4 (hexa)
    	 * (2) Convert these divisions into octal or hexa
    	 * ex: 10111 [base 2] => 10 | 111 = 27 [base 8]
    	 */
    	int somenumcopy = somenum, arrsize = 0;
    	while (somenumcopy > 0){ //To get the size of the array(arrsize), get how many divisions we will perform
    		if (base == 8)
    			somenumcopy = somenumcopy/1000;
    		else if (base == 16)
    			somenumcopy = somenumcopy/10000;
    		arrsize++;
    	}
    	int digit, i = 0;
    	String digitString;
    	String[] array = new String[arrsize];
    	while (somenum > 0){
    		if (base == 8){
    			digit = somenum%1000; //(1): Remove the last 3 numbers of somenum
    			somenum = somenum/1000; //Remove digit from somenum
    			digitString = String.valueOf(digit); //convert digit to string
        		array[i] = binary_into_threeOcts_or_fourHex_equivalent(digitString, 8); //(2): convert digit into binary equivalent
        		i++;
    		}
    		else if (base == 16){
    			digit = somenum%10000; //(1): Remove the last 4 numbers of somenum
    			somenum = somenum/10000; //Remove digit from somenum
    			digitString = String.valueOf(digit); //convert digit to string
        		array[i] = binary_into_threeOcts_or_fourHex_equivalent(digitString, 16); //(2): convert digit into binary equivalent
        		i++;
        		
    		}
    	}
    	String value = "";
    	for (int q = arrsize-1; q >= 0; q--){ //Combines array elements into 1 string value
    		value += array[q];
    	}
    	return value;
    }
    public String binary_into_threeOcts_or_fourHex_equivalent(String somenum, int base){
    	//THIS FUNCTION GETS SOMENUM AND CONVERTS IT INTO ITS EQUIVALENT BINARY VALUE DEPENDING ON THE BASE VARIABLE
    	String result = "";
    	if (base == 8){
                switch (somenum) {
                    case "000":
                    case "00":
                    case "0":
                        result = "0";
                        break;
                    case "001":
                    case "01":
                    case "1":
                        result = "1";
                        break;
                    case "010":
                    case "10":
                        result = "2";
                        break;
                    case "011":
                    case "11":
                        result = "3";
                        break;
                    case "100":
                        result = "4";
                        break;
                    case "101":
                        result = "5";
                        break;
                    case "110":
                        result = "6";
                        break;
                    case "111":
                        result = "7";
                        break;
                    default:
                        break;
                }
    	}
    	else if (base == 16){
                switch (somenum) {
                    case "0000":
                    case "000":
                    case "00":
                    case "0":
                        result = "0";
                        break;
                    case "0001":
                    case "001":
                    case "01":
                    case "1":
                        result = "1";
                        break;
                    case "0010":
                    case "010":
                    case "10":
                        result = "2";
                        break;
                    case "0011":
                    case "011":
                    case "11":
                        result = "3";
                        break;
                    case "0100":
                    case "100":
                        result = "4";
                        break;
                    case "0101":
                    case "101":
                        result = "5";
                        break;
                    case "0110":
                    case "110":
                        result = "6";
                        break;
                    case "0111":
                    case "111":
                        result = "7";
                        break;
                    case "1000":
                        result = "8";
                        break;
                    case "1001":
                        result = "9";
                        break;
                    case "1010":
                        result = "A";
                        break;
                    case "1011":
                        result = "B";
                        break;
                    case "1100":
                        result = "C";
                        break;
                    case "1101":
                        result = "D";
                        break;
                    case "1110":
                        result = "E";
                        break;
                    case "1111":
                        result = "F";
                        break;
                    default:
                        break;
                }
    	}
    	return result; 	
    }
    public Map decimal(String input){
        int convertnum = Integer.parseInt(input);
        Map<String, String> stringresult = new HashMap<>();
        
        stringresult.put("binary", this.convertFromDec(convertnum, 2));
        stringresult.put("octal", String.valueOf(this.convertFromDec(convertnum, 8)));
        stringresult.put("hexadecimal", this.convertFromDec(convertnum, 16));
        
    	return stringresult;
    }
    public String convertFromDec(int somenum, int base){
    	/* THIS FUNCTION CONVERTS DECIMAL INTO BINARY, OCTAL OR HEX
    	 * Steps:
    	 * (1) Divide number by given base (2, 8 or 16) and get the modulo
    	 * (2) Flip modulo into reverse 
    	 */
    	int somenumcopy = somenum, arraysize = 0, i = 0;
    	String value = "";
    	while (somenumcopy != 0){ //To get the arraysize, get the number of divisions we will perform
    		somenumcopy = somenumcopy/base;
    		arraysize++;
    	}
    	if (base != 16){
	    	int[] array = new int[arraysize];
	    	while (somenum != 0){
	    		array[i] = somenum%base; //(1)
	    		somenum = somenum/base;
	    		i++;
	    	}
	    	for (int q = arraysize-1; q >= 0; q--){ //(2): Combines array elements into string Value in reverse position
	    		value += array[q];
	    	}
    	} else if (base == 16){
        	int modulo;
        	char[] array = new char[arraysize];
        	while (somenum != 0){
        		modulo = somenum%16;
                    switch (modulo) {
                        case 1:
                            array[i] = '1';
                            break;
                        case 2:
                            array[i] = '2';
                            break;
                        case 3:
                            array[i] = '3';
                            break;
                        case 4:
                            array[i] = '4';
                            break;
                        case 5:
                            array[i] = '5';
                            break;
                        case 6:
                            array[i] = '6';
                            break;
                        case 7:
                            array[i] = '7';
                            break;
                        case 8:
                            array[i] = '8';
                            break;
                        case 9:
                            array[i] = '9';
                            break;
                        case 10:
                            array[i] = 'A';
                            break;
                        case 11:
                            array[i] = 'B';
                            break;
                        case 12:
                            array[i] = 'C';
                            break;
                        case 13:
                            array[i] = 'D';
                            break;
                        case 14:
                            array[i] = 'E';
                            break;
                        case 15:
                            array[i] = 'F';
                            break;
                        default:
                            break;
                    }
        		somenum = somenum/16;
        		i++;
        	}
        	for (int q = arraysize-1; q >= 0; q--){ //(2): Combines array elements into string Value in reverse position
	    		value += array[q];
	    	}
    	}
    	return value;
    }
    public Map octal(String input){
        
        int convertnum, result;
        Map<String, String> stringresult = new HashMap<>();
        if(!input.matches("^[1-7][0-7]*$") ||  input.isEmpty() ){
           // If the leading 0 requirement is there you can use the regex:
           // ^0[1-7][0-7]*$
            stringresult.put("error", "Invalid octal input.");
        }else{
            convertnum = Integer.parseInt(input);
            stringresult.put("decimal", String.valueOf(this.convertToDec(convertnum, 8)));
            stringresult.put("binary", String.valueOf(this.octToBin(convertnum, 8)));
            stringresult.put("hexadecimal",this.octToHex(convertnum));
        }
        
        return stringresult;
    }
    public int octToBin(int somenum, int base){
    	/* THIS FUNCTION CONVERTS OCTAL INTO BINARY
    	 * Steps:
    	 * (1) Convert each digit into binary equivalent
    	 */
    	int somenumcopy = somenum;
    	int arrsize = 0;
    	while (somenumcopy > 0){ //To get array size, get total number of digits of somenum
    		somenumcopy = somenumcopy/10;
    		arrsize++;
    	}
    	int digit;
    	int i = 0;
    	String[] array = new String[arrsize];
    	while (somenum > 0){
    		digit = somenum %10;
    		somenum = somenum/10;
    		array[i] = octal_or_hex_into_binaryEquivalent(digit, 'a', base);//(1)
    		i++;
    	}
    	String value = "";
    	for (int q = arrsize-1; q >= 0; q--){ //Combines array elements into 1 string value
    		value += array[q];
    	}
    	int someInt = Integer.parseInt(value); //Converts string into int someInt
    	return someInt;
    }
    public String octToHex(int somenum){
    	/* THIS FUNCTION CONVERTS OCTAL INTO HEXADECIMAL
    	 * Steps:
    	 * (1) Convert octal into binary [i.e. convert each digit into binary equivalent]
    	 * (2) Convert binary into hex [i.e. divide number by 4, then convert divisions into hex equivalent]
    	 */
    	int someInt = octToBin(somenum, 8);//(1)
    	String result = convertFromBin(someInt, 16);//(2)
    	return result;
    }
    public String octal_or_hex_into_binaryEquivalent(int somenumint, char somechar, int base){
    	String result = "";
    	if (base == 8){
                switch (somenumint) {
                    case 0:
                        result = "000";
                        break;
                    case 1:
                        result = "001";
                        break;
                    case 2:
                        result = "010";
                        break;
                    case 3:
                        result = "011";
                        break;
                    case 4:
                        result = "100";
                        break;
                    case 5:
                        result = "101";
                        break;
                    case 6:
                        result = "110";
                        break;
                    case 7:
                        result = "111";
                        break;
                    default:
                        break;
                }
    	}
    	else if (base == 16){
                switch (somechar) {
                    case '0':
                        result = "0000";
                        break;
                    case '1':
                        result = "0001";
                        break;
                    case '2':
                        result = "0010";
                        break;
                    case '3':
                        result = "0011";
                        break;
                    case '4':
                        result = "0100";
                        break;
                    case '5':
                        result = "0101";
                        break;
                    case '6':
                        result = "0110";
                        break;
                    case '7':
                        result = "0111";
                        break;
                    case '8':
                        result = "1000";
                        break;
                    case '9':
                        result = "1001";
                        break;
                    case 'A':
                        result = "1010";
                        break;
                    case 'B':
                        result = "1011";
                        break;
                    case 'C':
                        result = "1100";
                        break;
                    case 'D':
                        result = "1101";
                        break;
                    case 'E':
                        result = "1110";
                        break;
                    case 'F':
                        result = "1111";
                        break;
                    default:
                        break;
                }
    	}
    	return result; 	
    }
    public Map hexadecimal(String input){//
        
        int convertnum, result;
        Map<String, String> stringresult = new HashMap<>();
        if(!input.matches("-?[0-9a-fA-F]+") ||  input.isEmpty() ){
            stringresult.put("error", "Invalid hexadecimal input.");
        }else{
//            convertnum = Integer.parseInt(input);
            stringresult.put("decimal", String.valueOf(this.hexToDec(input)));
            stringresult.put("binary", String.valueOf(this.hexToBin(input)));
            stringresult.put("hexadecimal",this.hexToOct(input));
        }
        
        return stringresult;

    }
    public int hexToBin(String somenum){
    	/* THIS FUNCTION CONVERTS HEX INTO BINARY
    	 * Steps:
    	 * (1) Convert each digit into binary equivalent
    	 */
    	char somechar;
    	String value = "";
    	String[] array = new String[somenum.length()];
    	for(int x = 0; x < somenum.length(); x++) {
    	    somechar = somenum.charAt(x);
    	    String digit = octal_or_hex_into_binaryEquivalent(0, somechar, 16);
            array[x] = digit;
        }
    	for (int q = 0; q < somenum.length(); q++){
    		value += array[q];
    	}
    	int someInt = Integer.parseInt(value); //Converts string into int someInt
    	return someInt;
    }
    public int hexToDec(String somenum){
    	char somechar;
    	int value = 0;
    	int[] array = new int[somenum.length()];
    	for(int x = 0; x < somenum.length(); x++) {
    	    somechar = somenum.charAt(x);
            array[x] = hexChara_Into_DecimalEquivalent(somechar);
        }
    	for (int q = 0; q < somenum.length(); q++){
    		value += array[q]; //adds the values in the array
    	}
    	return value;
    }
    public int hexChara_Into_DecimalEquivalent(char somechar){
    	int result = 0;
        switch (somechar) {
            case 'A':
            case 'a':
                result = 10;
                break;
            case 'B':
            case 'b':
                result = 11;
                break;
            case 'C':
            case 'c':
                result = 12;
                break;
            case 'D':
            case 'd':
                result = 13;
                break;
            case 'E':
            case 'e':
                result = 14;
                break;
            case 'F':
            case 'f':
                result = 15;
                break;
            default:
                break;
        }
    	return result;
    }
    public String hexToOct(String somenum){
    	/* THIS FUNCTION CONVERTS HEX INTO OCTAL
    	 * Steps:
    	 * (1) Convert hex into binary [i.e. convert each digit into binary equivalent]
    	 * (2) Convert binary into octal [i.e. divide number by 3, then convert divisions into octal equivalent]
    	 */
    	char somechar;
    	String[] array = new String[somenum.length()];
    	for(int x = 0; x < somenum.length(); x++) {
    	    somechar = somenum.charAt(x);
    	    String digit = octal_or_hex_into_binaryEquivalent(0, somechar, 16); //(1)
            array[x] = digit;
        }
    	String value = "";
    	for (int q = 0; q < somenum.length(); q++){
    		value += array[q]; //converts String[] array into 1 string value
    	}
    	int someInt = Integer.parseInt(value); //converts hex string 'value' into int 'someInt' (binary)
    	String result = convertFromBin(someInt, 8);//(2)
    	return result;
    }

    public String calculateBit(int addressbits, int wordlength){
        int bitcapacity, bytecapacity, bitaddress, byteaddress, totaladdressbit = addressbits;
        String result = "";
        bytecapacity = exponent(wordlength/8);
        byteaddress = bytecapacity + addressbits;
        bitcapacity = exponent(wordlength);
        bitaddress = bitcapacity + addressbits;
        result += "<html><strong>Word Capacity</strong> = 2<sup>"+addressbits+"</sup> &nbsp;&nbsp;<strong>Word Address</strong> = "+totaladdressbit;
        result += "<br><strong>Byte Capacity</strong> = 2<sup>"+addressbits+"</sup> x 2<sup>" + bytecapacity+"</sup> &nbsp;&nbsp;<strong>Byte Address</strong> = "+byteaddress;
        result += "<br><strong>Bit Capacity</strong> = 2<sup>"+addressbits+"</sup> x 2<sup>" + bitcapacity+"</sup> &nbsp;&nbsp;<strong>Bit Address</strong> = "+bitaddress;
        return result+"</html>";
    }

    static int exponent(int somenum){
        int exponent = 0;
        while (somenum > 1){
            somenum = somenum/2;
            exponent++;
        }
        return exponent;
    }
}
